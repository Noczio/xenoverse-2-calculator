# DBZ Xenoverse 2 Character Attribute Calculator

Webpage for desktop made with Flask and the Fullpage.js plugin.

## Description

This project is based on the Xenoverse 2 calculator sheet avaliable in <https://docs.google.com/spreadsheets/d/1dBrxpq_7sNkIaTUJ_k1ow24lr-FDgEyQt7KGWIKHFpQ/copy>. Please refer to that calculator for a more extensive application that allows for transformations and boost.

## Why use this?

This particular attributes calculator has an easy to use UI for desktop devices that allows to check a xenoverse 2 character stats after selecting what race, body, height and other variables like qq bang and basic attributes. Moreover, I decided to keep it simple so any of your friends or sibling can try their own builds.

On the other hand, you're free to contribute to this project through Gitlab and improve the app for a better UX and quality. Create and issue and upload your code so we can merge it with the current commits.


## Installation

1. Go to https://drive.google.com/drive/folders/1mnvT6ZbJ7yt_0sJbMaB3NQoyz-RwQ5Lj?usp=drive_link and download any of the versions avaliable. Both work the same, except the lite version has almost all the files inside the EXE.

2. Open app.exe

3. Not working? try compiling it yourself with the instructions bellow.

## Compile project yourself

1. Clone the repository and open a terminal where it was downloaded, then use the conda command listed bellow (It's recommended to use Anaconda for managing your venv).

* conda env create -f environment.yml

2. Second, activate the created env. By default the new is always xenoverse-calculator.

* conda activate xenoverse-calculator

3. Finally, compile it with pyinstaller with the following command:

 * pyinstaller -w --add-data "templates;templates" --add-data "static;static" --add-data "start;start" --add-data "model;model" --add-data "error;error" --add-data "view;view" app.py
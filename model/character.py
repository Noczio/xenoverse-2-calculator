from flask_wtf import FlaskForm
from wtforms import SelectField, IntegerField, ValidationError
from wtforms.validators import InputRequired, NumberRange
from error.exception import InvalidAttributesException


class Character(FlaskForm):
    height = SelectField("Height", choices=[
        ("Shortest", "Shortest"), ("Short", "Short"), ("Tall", "Tall"), ("Tallest", "Tallest")])
    body = SelectField("Body", choices=[
        ("Thin", "Thin"), ("Medium", "Medium"), ("Thick", "Thick")])
    race = SelectField("Race", choices=[
        ("Frieza race", "Frieza race"),
        ("Namekian", "Namekian"),
        ("Female majin", "Female majin"),
        ("Male majin", "Male majin"),
        ("Female saiyan", "Female saiyan"),
        ("Male saiyan", "Male saiyan"),
        ("Female earthling", "Female earthling"),
        ("Male earthling", "Male earthling")])

    qq_health = IntegerField("Health", validators=[InputRequired(
    ), NumberRange(min=-5, max=5)], default=5)
    qq_ki = IntegerField("Ki", validators=[InputRequired(
    ), NumberRange(min=-5, max=5)], default=5)
    qq_stamina = IntegerField("Stamina", validators=[InputRequired(
    ), NumberRange(min=-5, max=5)], default=0)
    qq_basic_attack = IntegerField("Basic attack", validators=[InputRequired(
    ), NumberRange(min=-5, max=5)], default=5)
    qq_strike_supers = IntegerField("Strike supers", validators=[InputRequired(
    ), NumberRange(min=-5, max=5)], default=0)
    qq_ki_supers = IntegerField("Ki supers", validators=[InputRequired(
    ), NumberRange(min=-5, max=5)], default=5)

    health = IntegerField("Health", validators=[InputRequired(
    ), NumberRange(min=0, max=125)], default=83)
    ki = IntegerField("Ki", validators=[InputRequired(
    ), NumberRange(min=0, max=125)], default=42)
    stamina = IntegerField("Stamina", validators=[InputRequired(
    ), NumberRange(min=0, max=125)], default=0)
    basic_attack = IntegerField("Basic attack", validators=[InputRequired(
    ), NumberRange(min=0, max=125)], default=125)
    strike_supers = IntegerField("Strike supers", validators=[InputRequired(
    ), NumberRange(min=0, max=125)], default=0)
    ki_supers = IntegerField("Ki supers", validators=[InputRequired(
    ), NumberRange(min=0, max=125)], default=125)

    def validate(self):
        total_stats = self.health.data + self.ki.data + self.stamina.data + \
            self.basic_attack.data + self.strike_supers.data + self.ki_supers.data

        if total_stats < 0 or total_stats > 375:
            raise InvalidAttributesException()

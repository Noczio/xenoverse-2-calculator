import json
import math


class TotalComputation():
    def __init__(self, user_variables: dict, external_variables_path: str = "start/variables.json") -> None:
        self.user_variables = user_variables
        with open(external_variables_path) as user_file:
            self.external_variables = json.load(user_file)

    def health(self):
        race_coefficient = self.external_variables["Race"][self.user_variables["Race"]]
        height_coefficient = self.external_variables["Height"][self.user_variables["Height"]]
        character_health = self.user_variables["Health"]
        qq_bang_health = self.user_variables["QQ health"]
        constant_race = self.external_variables["Constant"]["Race"]
        constant_height = self.external_variables["Constant"]["Height"]
        contant_ap = self.external_variables["Constant"]["AP"]
        contant_clothing = self.external_variables["Constant"]["QQ multiplier"] * \
            self.external_variables["Constant"]["Clothing"]
        constant_health = self.external_variables["Constant"]["Health"]

        return constant_health + race_coefficient * constant_race + height_coefficient * constant_height + character_health * contant_ap + qq_bang_health * contant_clothing

    def ki(self):
        character_ki = self.user_variables["Ki"]
        qq_bang_ki = self.user_variables["QQ ki"]
        constant_bar = self.external_variables["Constant"]["Base"]["Bar"]
        constant_ap = self.external_variables["Constant"]["Base"]["AP"]
        contant_clothing = self.external_variables["Constant"]["QQ multiplier"] * \
            self.external_variables["Constant"]["Base"]["Clothing"]

        return math.floor(constant_bar + character_ki * constant_ap + qq_bang_ki * contant_clothing)

    def stamina(self):
        character_stamina = self.user_variables["Stamina"]
        qq_bang_stamina = self.user_variables["QQ stamina"]
        constant_bar = self.external_variables["Constant"]["Base"]["Bar"]
        constant_ap = self.external_variables["Constant"]["Base"]["AP"]
        contant_clothing = self.external_variables["Constant"]["QQ multiplier"] * \
            self.external_variables["Constant"]["Base"]["Clothing"]

        return math.floor(constant_bar + character_stamina * constant_ap + qq_bang_stamina * contant_clothing)

    def basic_attack(self):
        character_modifier = self.external_variables["Modifiers"][self.user_variables["Race"]]
        weigh_modifier = self.external_variables["Weight"][self.user_variables["Body"]]
        character_basic_attack = self.user_variables["Basic attack"]
        qq_bang_basic_attack = self.user_variables["QQ basic attack"]
        constanat_weight = self.external_variables["Constant"]["Weight"]["Base"]
        constant_ap = self.external_variables["Constant"]["Weight"]["AP"]
        constant_arrow = self.external_variables["Constant"]["QQ multiplier"] * \
            self.external_variables["Constant"]["Weight"]["Arrow"]
        constant_level_mod = self.external_variables["Constant"]["Level mod"]

        return constant_level_mod * (character_modifier["BA"] + weigh_modifier["BA"] * constanat_weight + constant_ap * character_basic_attack + qq_bang_basic_attack * constant_arrow)

    def strike_supers(self):
        character_modifier = self.external_variables["Modifiers"][self.user_variables["Race"]]
        weigh_modifier = self.external_variables["Weight"][self.user_variables["Body"]]
        character_strike_supers = self.user_variables["Strike supers"]
        qq_bang_strike_supers = self.user_variables["QQ strike supers"]
        constanat_weight = self.external_variables["Constant"]["Weight"]["Base"]
        constant_ap = self.external_variables["Constant"]["Weight"]["AP"]
        constant_arrow = self.external_variables["Constant"]["QQ multiplier"] * \
            self.external_variables["Constant"]["Weight"]["Arrow"]
        constant_level_mod = self.external_variables["Constant"]["Level mod"]

        return constant_level_mod * (character_modifier["SS"] + weigh_modifier["SS"] * constanat_weight + constant_ap * character_strike_supers + qq_bang_strike_supers * constant_arrow)

    def ki_supers(self):
        character_modifier = self.external_variables["Modifiers"][self.user_variables["Race"]]
        weigh_modifier = self.external_variables["Weight"][self.user_variables["Body"]]
        character_ki_supers = self.user_variables["Ki supers"]
        qq_bang_ki_supers = self.user_variables["QQ ki supers"]
        constanat_weight = self.external_variables["Constant"]["Weight"]["Base"]
        constant_ap = self.external_variables["Constant"]["Weight"]["AP"]
        constant_arrow = self.external_variables["Constant"]["QQ multiplier"] * \
            self.external_variables["Constant"]["Weight"]["Arrow"]
        constant_level_mod = self.external_variables["Constant"]["Level mod"]

        return constant_level_mod * (character_modifier["KBS"] + weigh_modifier["KBS"] * constanat_weight + constant_ap * character_ki_supers + qq_bang_ki_supers * constant_arrow)

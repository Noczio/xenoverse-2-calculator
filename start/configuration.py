DEVELOP = False


class Config(object):
    SECRET_KEY = "ahw0ozoh7fogieY"


class DevelopmentConfig(Config):
    ENVIRONMENT = "development"
    DEBUG = True


class RealUsageConfig(Config):
    ENVIRONMENT = "production"
    DEBUG = False


class DeploymentConfig():

    _config = {True: DevelopmentConfig,
               False: RealUsageConfig}

    @classmethod
    def initialize(cls):
        return cls._config[DEVELOP]

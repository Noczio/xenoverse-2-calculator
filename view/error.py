from error.exception import UnauthorizedException, InvalidAttributesException

from flask import redirect, url_for, flash, Blueprint


error = Blueprint('error', __name__, template_folder='templates')


@error.app_errorhandler(401)
@error.app_errorhandler(405)
@error.app_errorhandler(UnauthorizedException)
def unauthorized_error(error):
    flash("Error: Unauthorized response. A character must be created first.")
    return redirect(location=url_for("home.index"))


@error.app_errorhandler(InvalidAttributesException)
def invalid_attributes_error(error):
    flash("Error: Attributes summatory range is 0 to 375.")
    return redirect(location=url_for("home.index"))


@error.app_errorhandler(404)
def invalid_route_error(error):
    flash("Error: Invalid URL.")
    return redirect(location=url_for("home.index"))

from flask import redirect, url_for, render_template, request, session, Blueprint, get_flashed_messages
from model.character import Character


home = Blueprint('home', __name__, template_folder='templates')


@home.route("/", methods=["GET", "POST"])
def index():
    character_form = Character()
    flashed_messages = get_flashed_messages()
    session.clear()

    if request.method == "GET":
        return render_template("home.html", form=character_form, flashed_messages=flashed_messages)

    character_form.validate()

    session["User variables"] = {
        "Height": character_form.height.data,
        "Body": character_form.body.data,
        "Race": character_form.race.data,
        "Health": character_form.health.data,
        "Ki": character_form.ki.data,
        "Stamina": character_form.stamina.data,
        "Basic attack": character_form.basic_attack.data,
        "Strike supers": character_form.strike_supers.data,
        "Ki supers": character_form.ki_supers.data,
        "QQ health": character_form.qq_health.data,
        "QQ ki": character_form.qq_ki.data,
        "QQ stamina": character_form.qq_stamina.data,
        "QQ basic attack": character_form.qq_basic_attack.data,
        "QQ strike supers": character_form.qq_strike_supers.data,
        "QQ ki supers": character_form.qq_ki_supers.data
    }

    session["No extras"] = {
        "Height": character_form.height.data,
        "Body": character_form.body.data,
        "Race": character_form.race.data,
        "Health": 0,
        "Ki": 0,
        "Stamina": 0,
        "Basic attack": 0,
        "Strike supers": 0,
        "Ki supers": 0,
        "QQ health": 0,
        "QQ ki": 0,
        "QQ stamina": 0,
        "QQ basic attack": 0,
        "QQ strike supers": 0,
        "QQ ki supers": 0
    }

    return redirect(location=url_for("calc.compute"), code=307)

from flask import  render_template, redirect, url_for, session, Blueprint
from error.exception import UnauthorizedException


final = Blueprint('final', __name__, template_folder='templates')


@final.route("/result", methods=["GET"])
def result():
    if session.get("Total") and session.get("Base"):
        return render_template("final.html", total=session["Total"], base=session["Base"])
    
    raise UnauthorizedException()

@final.route("/result", methods=["POST"])
def go_back(): 
    return redirect(location=url_for("home.index"))
        

from flask import redirect, url_for, session, Blueprint
from error.exception import UnauthorizedException

from model.total import TotalComputation


calc = Blueprint('calc', __name__, template_folder='templates')


@calc.route("/compute", methods=["POST"])
def compute():
    if session.get("User variables") and session.get("No extras"):
        total = TotalComputation(session["User variables"])
        base = TotalComputation(session["No extras"])

        session["Total"] = {
            "Health": total.health(),
            "Ki": total.ki(),
            "Stamina": total.stamina(),
            "Basic attack": total.basic_attack(),
            "Strike supers": total.strike_supers(),
            "Ki supers": total.ki_supers(),
        }

        session["Base"] = {
            "Health": base.health(),
            "Ki": base.ki(),
            "Stamina": base.stamina(),
            "Basic attack": base.basic_attack(),
            "Strike supers": base.strike_supers(),
            "Ki supers": base.ki_supers(),
        }

        return redirect(location=url_for("final.result"))

    raise UnauthorizedException()

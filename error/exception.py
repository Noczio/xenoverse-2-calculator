class UnauthorizedException(Exception):
    pass


class InvalidAttributesException(Exception):
    pass

from view.final import final
from view.home import home
from view.calc import calc
from view.error import error
from flask import Flask
from flaskwebgui import FlaskUI

from start.configuration import DeploymentConfig, DEVELOP


def create_app():
    app = Flask(__name__)
    ui = FlaskUI(app=app, server="flask")
    app.config.from_object(DeploymentConfig.initialize())
    app.register_blueprint(home)
    app.register_blueprint(calc)
    app.register_blueprint(final)
    app.register_blueprint(error)

    return app, ui


if __name__ == "__main__":
    app, ui = create_app()
    if DEVELOP:
        app.run()
    else:
        ui.run()
